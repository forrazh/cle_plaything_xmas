- [Introduction](#orgd722537)
  - [Hardware/Software platform](#orgdb8442d)
  - [Seeed studio tutorials](#org99a507e)
  - [FreeRTOS](#org685a8ed)
- [Project](#org8be5eb5)
  - [List of sensors and actuators used in the project](#org32b05a3)
  - [Description](#org10d2f17)



<a id="orgd722537"></a>

# Introduction


<a id="orgdb8442d"></a>

## Hardware/Software platform

The following exercises will be done on a Arduino Uno platform. To program the platform, you need to use one of the two IDEs :

-   The Arduino IDE (which is already available on the machines in A11)

-   [Platform IO](https://docs.platformio.org/en/latest//what-is-platformio.html), which is available as a plugin of VSCode. Please follow the instruction on the web site to install it on the machines of A11.

I personnally use PlatformIO, and I recommend you to do the same.


<a id="org99a507e"></a>

## Seeed studio tutorials

The sensors and actuators that we will use are part of the Seeed Studio [Grove Arduino Starter Kit](https://www.seeedstudio.com/Grove-Starter-Kit-for-Arduino-p-1855.html).

You can find some video lectures on how to use the Seeed Studio Shield and the kit :

-   <https://www.youtube.com/watch?v=GfgSyCZMb_g&ab_channel=CreateLabzStore>

Here are the Starter Kit examples that you can download and try on the board.

-   <https://github.com/Seeed-Studio/Sketchbook_Starter_Kit_for_Arduino>


<a id="org685a8ed"></a>

## FreeRTOS

For the project, we will use the FreeRTOS real-time operating system. The complete documentation is available [here](https://freertos.org/FreeRTOS-quick-start-guide.html). In particular, look at the developers doc.

To install FreeRTOS for your project, the procedure is sligthly different for the Arduino IDE and for the PlatformIO.

-   [Using FreeRTOS on the Arduino IDE](https://www.arduino.cc/reference/en/libraries/freertos/)
-   [Using FreeRTOS on PlatformIO](https://docs.platformio.org/en/latest/frameworks/freertos.html)


<a id="org8be5eb5"></a>

# Project


<a id="org32b05a3"></a>

## List of sensors and actuators used in the project

-   The button;
-   the buzzer;
-   the display;
-   the light sensor;
-   the step motor;
-   the temperature sensor;
-   the potentiometer.


<a id="org10d2f17"></a>

## Description

The objective of this student project is to put into practice almost everything we have seen so far in the course in a real embedded system. You will develop a prototype of a small gadget for Christmas with an Arduino board.

The application has four operating modes.

1.  In the first mode `Time` the display shows the number of seconds since the device has been reset.

2.  In the second mode `Temp`, the display shows the value of the temperature sensor (possibly in Celsius degrees).

3.  In the third mode `Angle`, the display shows the values coming from the potentiometer and uses the step motor.

4.  In the fourth mode `Christmas` :
    -   the buzzer starts to play a Christmas tune (for example, Jingle Bells);
    -   the display shows the Christmas date: `25:12`.

The application changes from one mode to the other depending on the status of the button and of other sensors, according to the following state machine:

![img](application-sm.png)

The `button` event corresponds to a button press. The `light_off` event corresponds to the light sensor not detecting enough light anymore (you will need to set a threshold for the detected light). The `light_on` is when the light is sensor detects enough light.

When in mode `Angle`, the display shows the value of the potentiometer. In particular, this value should be discrete: the minimum value is 0 (corresponding to the potentiometer at its minimum resistance), and the maximum should be 9 (corresponding to its maximum resistance). When the user turns the potentiometer from one value to the next one, the buzzer should play a short "click" sound. For example, if the current value is a 4, and the user turns the potentiometer clockwise to reach value 5, then the buzzer plays a short "click" and shows "5" on the display. Additionally, the step motor turns anticlockwise when the counter decreases and clockwise when the counter increases.

Develop the application using Arduino and FreeRTOS. Since memory is **very limited**, you need to use a small number of threads (no more than 2 or 3). Use techniques similar to the one used for code-generation from StateCharts to properly organize your code.