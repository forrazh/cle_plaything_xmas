#include "Arduino.h"
#include "modes/AngleMode.hpp"
#include "modes/TempMode.hpp"
#include "modes/TimeMode.hpp"
#include "modes/XMasMode.hpp"
#include "modules/Buzzer.hpp"
#include "modules/HeatSensor.hpp"
#include "modules/PotentiometerAndMotor.hpp"
#include "rgb_lcd.hpp"

rgb_lcd lcd;

// Switchers
constexpr auto LIGHT_PIN = A3;
constexpr auto BUTTON_PIN = 6;

// Modules
Buzzer buzzer(lcd, 3);
HeatSensor heatSensor(lcd, A1);
PotentiometerAndMotor potentiometerAndMotor(lcd, A0);

// Modes
TimeMode timer(lcd);
TempMode thermo(lcd, heatSensor);
AngleMode angle(lcd, buzzer, potentiometerAndMotor);
XMasMode xmas(lcd, buzzer);

Mode *my_modes[]{
        &timer,
        &thermo,
        &angle,
};

// new -> delete
// smart ptr: std::unique_ptr

enum class Modes {
    Time,
    Temp,
    Angle,
    SIZE
};

int operator+(Modes m) {
    return static_cast<int>(m);
}

Modes &operator++(Modes &m) {
    m = static_cast<Modes>((+m + 1) % +Modes::SIZE);
    return m;
}

Modes current_mode = Modes::Time;
bool wasXmas = false;
int lastButtonPress = 0;

void setup() {
    //     set up the LCD's number of columns and rows:
    lcd.begin(16, 2);
    //     set up the pins used to switch modes
    pinMode(LIGHT_PIN, INPUT);
    pinMode(BUTTON_PIN, INPUT_PULLUP);
    //     set up the pin for the motor
    potentiometerAndMotor.attach(4);
}

void loop() {
    int lux = analogRead(LIGHT_PIN);

    if (lux > 200) {
        if (!wasXmas) {
            wasXmas = true;


            my_modes[+current_mode]->notifyOut();
        }
        xmas.run();
    } else {
        int button_pressed = digitalRead(BUTTON_PIN);
        if (wasXmas) {
            xmas.notifyOut();
            wasXmas = false;
        }
        if (button_pressed == 1) {
            if (lastButtonPress != button_pressed) {
                // Switch mode
                my_modes[+current_mode]->notifyOut();
                ++current_mode;
            }
        }
        lastButtonPress = button_pressed;

        // Run the correct one
        my_modes[+current_mode]->run();
    }
}
