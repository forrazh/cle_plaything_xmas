//
// Created by dra on 12/3/22.
//

#pragma once

#include "../Module.hpp"
#include <Servo.h>

// const int READ_FROM = A1;
const int MOTOR = 4;

const int MAX_SERVO_VALUE = 166;
const int MAX_STEP_NUMBER = 9;

class PotentiometerAndMotor : public Module
{
private:
  Servo servo;

public:
  explicit PotentiometerAndMotor(rgb_lcd& lcd, int pin) : Module(lcd, pin, INPUT) {}
  uint8_t attach(int pin)
  {
    return servo.attach(pin);
  }
  int use()
  {
    const int value = (int)(analogRead(pin) / 110 /* / 6.15*/);
    const int angle = (int)(value * MAX_SERVO_VALUE / MAX_STEP_NUMBER);

    lcd.setCursor(0, 1);
    lcd.print("      ");
    lcd.setCursor(0, 1);
    lcd.print(value);

    servo.write(MAX_SERVO_VALUE - angle);
    //        delay(15);
    return value;
  }
};
