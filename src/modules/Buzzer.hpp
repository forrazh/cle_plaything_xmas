//
// Created by dra on 12/2/22.
//

#pragma once

#include "../Module.hpp"

constexpr int  length = 15;
constexpr char notes[] = "ccggaagffeeddc";
constexpr int  beats[] = {1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 4};
constexpr int  tempo = 300;

class Buzzer : public Module
{
  int currIndex = 0;

private:
  void playTone(int tone, int duration)
  {
    for (long i = 0; i < duration * 1000L; i += tone * 2)
    {
      digitalWrite(pin, HIGH);
      delayMicroseconds(tone);
      digitalWrite(pin, LOW);
      delayMicroseconds(tone);
    }
  }

  void playNote(char note, int duration)
  {
    static constexpr char names[] = {'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C'};
    static constexpr int  tones[] = {1915, 1700, 1519, 1432, 1275, 1136, 1014, 956};

    for (int i = 0; i < 8; i++)
    {
      if (names[i] == note)
      {
        playTone(tones[i], duration);
      }
    }
  }

public:
  explicit Buzzer(rgb_lcd& lcd, int pin) : Module(lcd, pin, OUTPUT) {}

  void bip(int number)
  {
    const int tone = 1260 - number * 130;

    playTone(tone, 2);
  }

  // Can be ugraded with a counter and playin' only the current note at each call...
  int use()
  {
    if (currIndex >= length)
    {
      currIndex = 0;
    }

    if (notes[currIndex] == ' ')
    {
      delay(beats[currIndex] * tempo);
    }
    else
    {
      playNote(notes[currIndex], beats[currIndex] * tempo);
    }

    delay(tempo / 2);

    return currIndex++;
  }
};
