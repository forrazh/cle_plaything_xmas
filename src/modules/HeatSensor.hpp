//
// Created by dra on 12/3/22.
//

#pragma once

#include <math.h>

//#include <cmath>
#include "../Module.hpp"

constexpr int B = 3975;

class HeatSensor : public Module
{
public:
  explicit HeatSensor(rgb_lcd& lcd, int pin) : Module(lcd, pin, INPUT) {}
  void use()
  {
    const int   val = analogRead(pin);
    const float resistance = (float)(1023 - val) * 10000 / val;
    const double temperature = 1 / (log(resistance / 10000) / B + 1 / 298.15) - 273.15;

    lcd.setCursor(0, 0);
    lcd.print(temperature);

    delay(100);
  }
};
