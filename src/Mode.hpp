//
// Created by dra on 12/4/22.
//

#pragma once

#include "Arduino.h"
#include "rgb_lcd.hpp"

class Mode
{
protected:
  String   name;
  rgb_lcd& lcd;
  bool     justCameIn = true;
  Mode(String name, rgb_lcd& lcd) : name(name), lcd(lcd) {}

  virtual void run_impl() = 0;

public:
  void run()
  {
    if (justCameIn)
    {
      justCameIn = false;
      lcd.clear();
      const int l = name.length();
      lcd.setCursor(15 - l, 1);
      lcd.print(name);
    }

    run_impl();
  }

  virtual void notifyOut()
  {
    justCameIn = true;
  }
};
