//
// Created by dra on 12/2/22.
//

#pragma once

#include "Arduino.h"
#include "rgb_lcd.hpp"

class Module
{
public:
  rgb_lcd&  lcd;
  const int pin;

  Module(rgb_lcd& lcd, int pin, int mode) : lcd(lcd), pin(pin)
  {
    pinMode(pin, mode);
  }

  int use();
};
