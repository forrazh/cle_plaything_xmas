//
// Created by dra on 12/3/22.
//

#pragma once

#include "../Mode.hpp"
#include "../modules/Buzzer.hpp"

// Needs :
// Buzzer
// Display
class XMasMode : public Mode
{
  Buzzer& buzzer;
  bool    displayedX = false;

public:
  XMasMode(rgb_lcd& lcd, Buzzer& buzzer) : Mode("XMas", lcd), buzzer(buzzer) {}
  void run_impl() override
  {
    if (!displayedX)
    {
      displayedX = true;
      lcd.setCursor(3, 0);
      lcd.print("25/12 :)");
    }

    buzzer.use();
  }

  void notifyOut() override
  {
    Mode::notifyOut();
    displayedX = false;
  }
};
