//
// Created by dra on 12/3/22.
//

#pragma once

#include "../Mode.hpp"
#include "../modules/Buzzer.hpp"
#include "../modules/PotentiometerAndMotor.hpp"

// Needs :
// Potentiometer
// Motor
// Buzzer
// Display
class AngleMode : public Mode
{
  Buzzer&                buzzer;
  PotentiometerAndMotor& potentiometerAndMotor;
  int                    lastBip = -1;

public:
  AngleMode(rgb_lcd& lcd, Buzzer& buzzer, PotentiometerAndMotor& potentiometerAndMotor)
    : Mode("Angle", lcd), buzzer(buzzer), potentiometerAndMotor(potentiometerAndMotor)
  {
  }
  void run_impl() override
  {
    const int potentiometerVal = potentiometerAndMotor.use();
    if (lastBip != potentiometerVal)
    {
      lastBip = potentiometerVal;

//      lcd.setCursor(0, 0);
//      lcd.print(potentiometerVal);
      buzzer.bip(potentiometerVal);
    }
  }
};
