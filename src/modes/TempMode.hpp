//
// Created by dra on 12/3/22.
//

#pragma once

#include "../Mode.hpp"
#include "../modules/HeatSensor.hpp"

// Needs :
// HeatSensor
// Display
class TempMode : public Mode
{
  HeatSensor& heatSensor;

public:
  explicit TempMode(rgb_lcd& lcd, HeatSensor& heatSensor)
    : Mode("Temp", lcd), heatSensor(heatSensor)
  {
  }
  void run_impl() override
  {
    heatSensor.use();
  }
};
