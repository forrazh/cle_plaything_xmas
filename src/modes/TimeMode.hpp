//
// Created by dra on 12/3/22.
//

#pragma once

#include "../Mode.hpp"

// Needs :
// Display
class TimeMode : public Mode
{
  bool displayedT = false;

public:
  explicit TimeMode(rgb_lcd& lcd) : Mode("Timer", lcd) {}
  void run_impl() override
  {
    if (!displayedT)
    {
      displayedT = true;
      lcd.setCursor(0, 0);
      lcd.print("Time since last");
      lcd.setCursor(0, 1);
      lcd.print("reset :");
    }

    // int: complement a 2
    // double: norme IEEE 754: signe_exp_mentissa
    const int time = millis() / 1000;
    const int offset = static_cast<int>(log10(time));
    // static_cast<int>(1.5);
    // RTTI: RunTime Type Information <-- souvent désactivé en embarqué
    // (virtual/polymorphisme/exception/typeid)
    // exception <-- Encore plus souvent désactivé en embarqué

    // Mode* m = this;
    // dynamic_cast<TimeMode*>(m);

    // reinterpret_cast<byte*>(this);

    lcd.setCursor(15 - offset, 1);
    lcd.print(time);
  }

  //   NSDMI: non static data member initialization
  void notifyOut() override
  {
    Mode::notifyOut();
    displayedT = false;
  }
};
